/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * existence of helm tgz file validation
 */
public class ContainerHelmTgzFileValidation {
    private static final String INNER_EXCEPTION = "inner exception, please check the log.";

    private static final String TGZ_NOT_EXISTS = "there is no .tgz file in Artifacts/Deployment/Charts dir";

    private static final String VM = "vm";

    private static final String SUCCESS = "success";

    /**
     * execute test case.
     *
     * @param filePath csar file path
     * @param context context info
     * @return execute result
     */
    public String execute(String filePath, Map<String, String> context) {
        delay();
        //vm app does not has helm chart file.
        if (VM.equalsIgnoreCase(getAppType(filePath))) {
            return SUCCESS;
        }
        try (ZipFile zipFile = new ZipFile(filePath)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.getName().startsWith("Artifacts/Deployment/Charts") && entry.getName().endsWith(".tgz")) {
                    return SUCCESS;
                }
            }
        } catch (IOException e) {
            return INNER_EXCEPTION;
        }

        return TGZ_NOT_EXISTS;
    }

    /**
     * add delay.
     */
    private void delay() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
        }
    }

    /**
     * get app_type.
     *
     * @param filePath filePath
     * @return appType appType
     */
    private String getAppType(String filePath) {
        try (ZipFile zipFile = new ZipFile(filePath)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.getName().split("/").length == 1 && entry.getName().endsWith(".mf")) {
                    return analysizeMfAndGetAppClass(zipFile, entry);
                }
            }
        } catch (IOException e) {
        }
        return null;
    }

    /**
     * analysize mf file and get app class value.
     *
     * @param zipFile zipFile
     * @param entry entry
     * @return file type
     */
    private String analysizeMfAndGetAppClass(ZipFile zipFile, ZipEntry entry) {
        try (BufferedReader br = new BufferedReader(
            new InputStreamReader(zipFile.getInputStream(entry), StandardCharsets.UTF_8))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                // prefix: path
                if (line.trim().startsWith("app_class")) {
                    return line.split(":")[1].trim();
                }
            }
        } catch (IOException e) {
        }
        return null;
    }
}
